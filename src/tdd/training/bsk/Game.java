package tdd.training.bsk;

public class Game {
	
	Frame[] frames;
	int index = 0;
	private int firstBonusThrow = 0;
	private int secondBonusThrow = 0;

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		frames = new Frame[10];
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		if (index > 9) {
			throw new BowlingException("This game already has 10 frames.");
		}
		frames[index++] = frame;
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		if (index < 0 || index > 9) {
			throw new BowlingException("Index should be between 0 and 9 (inclusive).");
		}
		return frames[index];	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		if (firstBonusThrow < 0 || firstBonusThrow > 10) {
			throw new BowlingException("Must be a number between 0 and 10 (inclusive).");
		}
		this.firstBonusThrow = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		if (firstBonusThrow < 0 || firstBonusThrow > 10) {
			throw new BowlingException("Must be a number between 0 and 10 (inclusive).");
		}
		this.secondBonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int score = 0;
		for (int i = 0; i < 10; i++) {
			if (frames[i] == null) {
				throw new BowlingException("Game has less than 10 frames.");
			}
			
			if (i < 8 && frames[i].isStrike() && frames[i+1].isStrike()) {
				frames[i].setBonus(10 + frames[i+2].getFirstThrow());
			} else if (i == 8 && frames[8].isStrike() && frames[9].isStrike()) {
				frames[8].setBonus(10 + firstBonusThrow);
				frames[9].setBonus(firstBonusThrow + secondBonusThrow);
				firstBonusThrow = 0;
				secondBonusThrow = 0;
			} else if (i < 9) {
				if (frames[i].isSpare()) {
					frames[i].setBonus(frames[i+1].getFirstThrow());
				} else if (frames[i].isStrike()) {
					frames[i].setBonus(frames[i+1].getFirstThrow() + frames[i+1].getSecondThrow());
				}
			} 
			
			score += frames[i].getScore();
		}
		return score + firstBonusThrow + secondBonusThrow;	
	}

}
