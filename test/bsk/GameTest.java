package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.Frame;
import tdd.training.bsk.Game;

public class GameTest {

	@Test
	public void firstThrowOfFirstFrameShouldBe1() throws Exception {
		Game game = new Game();
		game.addFrame(new Frame(1, 5));
		assertEquals(1, game.getFrameAt(0).getFirstThrow());
	}
	
	@Test
	public void secondThrowOfFirstFrameShouldBe5() throws Exception {
		Game game = new Game();
		game.addFrame(new Frame(1, 5));
		assertEquals(5, game.getFrameAt(0).getSecondThrow());
	}
	
	@Test
	public void firstThrowOfSecondFrameShouldBe3() throws Exception {
		Game game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		assertEquals(3, game.getFrameAt(1).getFirstThrow());
	}
	
	@Test
	public void secondThrowOfSecondFrameShouldBe6() throws Exception {
		Game game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		assertEquals(6, game.getFrameAt(1).getSecondThrow());
	}
	
	@Test
	public void scoreShouldBe81() throws Exception {
		Game game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		assertEquals(81, game.calculateScore());
	}
	
	@Test
	public void frameShouldBeSpare() throws Exception {
		Game game = new Game();
		game.addFrame(new Frame(1, 9));
		game.addFrame(new Frame(3, 6));
		assertTrue(game.getFrameAt(0).isSpare());
	}
	
	@Test
	public void scoreShouldBe88() throws Exception {
		Game game = new Game();
		game.addFrame(new Frame(1, 9));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		assertEquals(88, game.calculateScore());
	}
	
	@Test
	public void frameShouldBeStrike() throws Exception {
		Game game = new Game();
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(3, 6));
		assertTrue(game.getFrameAt(0).isStrike());
	}
	
	@Test
	public void scoreShouldBe94() throws Exception {
		Game game = new Game();
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		assertEquals(94, game.calculateScore());
	}
	
	@Test
	public void scoreShouldBe103() throws Exception {
		Game game = new Game();
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(4, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		assertEquals(103, game.calculateScore());
	}
	
	@Test
	public void scoreShouldBe112() throws Exception {
		Game game = new Game();
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		assertEquals(112, game.calculateScore());
	}
	
	@Test
	public void scoreShouldBe98() throws Exception {
		Game game = new Game();
		game.addFrame(new Frame(8, 2));
		game.addFrame(new Frame(5, 5));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		assertEquals(98, game.calculateScore());
	}
	
	@Test
	public void firstBonusThrowShouldBe7() throws Exception {
		Game game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 8));
		game.setFirstBonusThrow(7);
		assertEquals(7, game.getFirstBonusThrow());
	}
	
	@Test
	public void scoreShouldBe90() throws Exception {
		Game game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 8));
		game.setFirstBonusThrow(7);
		assertEquals(90, game.calculateScore());
	}
	
	@Test
	public void secondBonusThrowShouldBe2() throws Exception {
		Game game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(10, 0));
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		assertEquals(2, game.getSecondBonusThrow());
	}
	
	@Test
	public void scoreShouldBe92() throws Exception {
		Game game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(10, 0));
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		assertEquals(92, game.calculateScore());
	}
	
	@Test
	public void frameScoreShouldBe300() throws Exception {
		Game game = new Game();
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
		assertEquals(300, game.calculateScore());
	}

}
